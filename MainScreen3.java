import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainScreen3 implements ActionListener {
	
	JLabel label;
	JButton button;

	public MainScreen3() {
		// Nieuw frame (window)
		JFrame frame = new JFrame("Hoofdscherm");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(400, 500); // width, height
		frame.setLayout(null);
		frame.setVisible(true);
	
		// Nieuwe knop
		button = new JButton("Klik");
		button.setBounds(130, 50, 100, 40);
		button.addActionListener(this);
		
		// Nieuw label (tekst op het scherm)
		label = new JLabel("Tekst");
		label.setBounds(150, 100, 100, 30); // x, y, width, height
		
		// Voeg de componenten toe aan het frame
		frame.add(button);
		frame.add(label);
		}
	
	public void actionPerformed(ActionEvent arg0) {
		
		if(label.getText() == "Tekst") {

			label.setText("Hallo label!");
			
		} else {
			
			label.setText("Tekst");
			
		}
		
	}

}
