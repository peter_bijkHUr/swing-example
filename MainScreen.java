import javax.swing.*;

public class MainScreen {
	
	JLabel label;
	JButton button;

	public MainScreen() {
		// Nieuw frame (window)
		JFrame frame = new JFrame("Hoofdscherm");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(400, 500); // breedte, hoogte
		frame.setLayout(null); // geen layout manager toepassen
		frame.setVisible(true); // maak het frame zichtbaar
	
		// Nieuwe knop
		button = new JButton("Klik");
		button.setBounds(130, 50, 100, 40); // x, y, breedte, hoogte
		
		// Nieuw label (tekst op het scherm)
		label = new JLabel("Tekst");
		label.setBounds(165, 100, 100, 30); // x, y, breedte, hoogte
		
		// Voeg de componenten toe aan het frame
		frame.add(button);
		frame.add(label);
		}
}